""" Fonctions utilitaires pour manipuler les matrices """

import API_matrice2 as matrice
#import API_matrice1 as matrice

def est_matrice_tuple():
    """nous permet de savoir si la matrice qui sera gérée par l'API importée est sous la forme d'un tuple ou d'une liste de liste
       pour cela on regarde si le type d'une matrice de hauteur 0,0 est tuple ou liste

    Returns:
        Booléen: True si la matrice gérée par l'API est sous la forme d'un tuple, False sinon
    """    
    return type(matrice.construit_matrice(0,0,0)) == tuple

def construit_matrice(nb_ligne, nb_colonnes, valeur_par_défaut=0):
    return matrice.construit_matrice(nb_ligne, nb_colonnes, valeur_par_défaut)
    
def set_val(matrice, ligne, colonne, nouvelle_valeur):
    matrice.set_val(matrice, ligne, colonne, nouvelle_valeur)

def get_nb_lignes(matrice):
    return matrice.get_nb_lignes(matrice)

def get_nb_colonnes(matrice):
    return matrice.get_nb_colonnes(matrice)

def get_val(matrice, ligne, colonne):
    return matrice.get_val(matrice,ligne,colonne)

def get_ligne(matrice, ligne):
    return matrice.get_ligne(matrice,ligne)

def get_colonne(matrice,colonne):
    return matrice.get_colonne(matrice,colonne)

def get_diagonale_principale(matrice):
    return matrice.get_diagonale_principale(matrice)

def get_diagonale_secondaire(matrice):
    return matrice.get_diagonale_secondaire(matrice)

def transposee(matrice):
    return matrice.transposee(matrice)

def is_triangulaire_inf(matrice):
    return matrice.is_triangulaire_inf(matrice)

def is_triangulaire_sup(matrice):
    return matrice.is_triangulaire_sup(matrice)

def somme(matrice1, matrice2):
    return matrice.somme(matrice1, matrice2)

def produit(matrice1, matrice2):
    return matrice.produit(matrice1, matrice2)

def bloc(matrice,ligne,colonne,hauteur,largeur):
    return matrice.bloc(matrice,ligne,colonne,hauteur,largeur)

def affiche_ligne_separatrice(mat,taille_celulle=4):
    matrice.affiche_ligne_separatrice(mat, taille_celulle)

def affiche(mat, taille_cellule=4):
    matrice.affiche(mat,taille_cellule)