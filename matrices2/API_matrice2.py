def construit_matrice(nb_lignes,nb_colonnes,valeur_par_defaut=0):
    """qui renvoie sous la forme d’une liste de listes notre matrice

    Args:
        nb_lignes (int): nombre de lignes de la matrice à créer
        nb_colonnes (int): nombre de colonnes de la matrice à créer
        valeur_par_defaut (int, optional): _description_. Defaults to 0.

    Returns:
        liste de listes: renvoie la nouvelle matrice créée
    """
    # j listes contenant i éléments avec la la valeur par défaut    
    return [[valeur_par_defaut for i in range(nb_colonnes)] for j in range(nb_lignes)]

def get_nb_lignes(matrice):
    """permet de connaître le nombre de lignes d’une matrice

    Args:
        matrice (liste de listes): une matrice 

    Returns:
        int: le nombre de lignes de la matrice
    """    
    return len(matrice)

def get_nb_colonnes(matrice):
    """permet de connaître le nombre de colonnes d’une matrice

    Args:
        matrice (liste de listes): une matrice

    Returns:
        int: le nombre de colonnes de la matrice
    """    
    if len(matrice) == 0:
        return 0
    return len(matrice[0])

def get_val(matrice,ligne,colonne):
    """nous permet de connaitre la valeur d'une matrice à une ligne et une colonne précise

    Args:
        matrice (liste de listes): une matrice
        ligne (int): indice de ligne
        colonne (int): indice de colonne

    Returns:
        int: la valeur de la matrice à une ligne et une colonne précise
    """    
    return matrice[ligne][colonne]

def set_val(matrice,ligne,colonne,nouvelle_valeur):
    """nous permet de définir la valeur d'une matrice à une ligne et une colonne précise

    Args:
        matrice (liste de listes): une matrice
        ligne (int): indice de ligne
        colonne (int): indice de colonne
        nouvelle_valeur (string/int) : nouvelle valeur à assigner à notre matrice aux indices en question
    """    
    matrice[ligne][colonne] = nouvelle_valeur

def get_ligne(matrice,ligne):
    """qui renvoie sous la forme d’une liste la ligne de la matrice dont le numéro est spécifié.

    Args:
        matrice (liste de listes): une matrice
        ligne (int): indice de ligne

    Returns:
        list: renvoie sous la forme d’une liste la ligne de la matrice dont le numéro est spécifié.
    """    
    if not ligne in range(len(matrice)):
        return []
    return matrice[ligne]

def get_colonne(matrice,colonne):
    """qui renvoie sous la forme d’une liste la colonne de la matrice dont le numéro est spécifié.

    Args:
        matrice (liste de listes): une matrice
        colonne (int): indice de colonne

    Returns:
        list: renvoie sous la forme d’une liste la colonne de la matrice dont le numéro est spécifié.
    """   
    if len(matrice) == 0 or not colonne in range(len(matrice[0])):
        return []
    #pour chaque ligne on ajoute l'élément à l'indice colonne dans notre liste
    return [matrice[i][colonne] for i in range(get_nb_lignes(matrice))]

def get_diagonale_principale(matrice):
    """qui renvoie sous la forme d’une liste la diagonale principale d’une matrice carrée.
    
    Args:
        matrice (liste de listes): une matrice

    Returns:
        list: renvoie sous la forme d’une liste la diagonale principale de la matrice carrée.
    """
    # on sait qu'un élément appartient à la diagonale principale si i = j donc on a:    
    return [matrice[i][i] for i in range(len(matrice))]

def get_diagonale_secondaire(matrice):
    """qui renvoie sous la forme d’une liste la diagonale secondaire d’une matrice carrée.
    
    Args:
        matrice (liste de listes): une matrice

    Returns:
        list: renvoie sous la forme d’une liste la diagonale secondaire de la matrice carrée.
    """  
    # pour i allant de 0 au nombre de lignes, on rajoute à notre liste l'élément de la matrice à la ligne i et à la colonne -i-1(-1 car i commence à 0)
    return [matrice[i][-i-1] for i in range(len(matrice))]

def transposee(matrice):
    """qui renvoie la transposée d’une matrice.

    Args:
       matrice (liste de listes): une matrice

    Returns:
       matrice (liste de listes): renvoie la transposée de la matrice.
    """ 
    # code en plusieurs lignes   
    # nouv_mat = []
    # for i in range(get_nb_colonnes(matrice)):
    #     nouv_mat.append(get_colonne(matrice,i))
    
    # pour chaque colonne, on rajoute une ligne contenant chaque valeur de la colonne
    return [get_colonne(matrice,i) for i in range(get_nb_colonnes(matrice))]

def is_triangulaire_inf(matrice):
    """qui indique si une matrice est triangulaire inférieure.

    Args:
        matrice (liste de listes): une matrice

    Returns:
        bool: indique si une matrice est triangulaire inférieure(True) ou non(False).
    """    
    # En regardant des exemples de matrices triangulaires inférieures sur internet, on remarque que les éléments égaux à 0 dans ces matrices
    # sont toujours à un indice de colonne supérieur à l'indice de ligne
    
    # for i in range(len(matrice)):
    #     for j in range(len(matrice[i])):
    #         if j>i and matrice[i][j] != None:
    #             return False
    # return True
    
    
    # une version en une ligne en dessous en utilisant la fonction all qui vérifie si tous les éléments d'une liste sont égaux à True ou non
    return all([element for sous_liste in [[matrice[i][j] == None for j in range(len(matrice[i])) if j > i] for i in range(len(matrice))] for element in sous_liste])

def is_triangulaire_sup(matrice):
    """qui indique si une matrice est triangulaire supérieure.

    Args:
        matrice (liste de listes): une matrice

    Returns:
        bool: indique si une matrice est triangulaire supérieure(True) ou non(False).
    """    
    # En regardant des exemples de matrices triangulaires supérieures sur internet, on remarque que les éléments égaux à 0 dans ces matrices
    # sont toujours à un indice de ligne inférieur ou égal à l'indice de la colonne
    
    # for i in range(len(matrice)):
    #     for j in range(len(matrice[i])):
    #         if i<=j and matrice[i][j] != None:
    #             return False
    # return True
    
    # une version en une ligne en dessous en utilisant la fonction all qui vérifie si tous les éléments d'une liste sont égaux à True ou non
    # element for sous_liste in .... permet "d'applatir" notre liste de liste en une liste simple afin de pouvoir utiliser cette fonction all
    return all([element for sous_liste in [[matrice[i][j] == None for j in range(len(matrice[i])) if j < i] for i in range(len(matrice))] for element in sous_liste])
    

def bloc(matrice,ligne,colonne,hauteur,largeur):
    """qui renvoie la sous-matrice
    de la matrice commençant à la ligne et colonne indiquées et dont les dimensions sont hauteur et
    largeur.

    Args:
        matrice (liste de listes): une matrice
        ligne (int): indice de ligne
        colonne (int): indice de colonne
        hauteur (int): hauteur désirée de la matrice
        largeur (int): largeur désirée de la matrice

    Returns:
        matrice (liste de listes): la sous-matrice
        de la matrice commençant à la ligne et colonne indiquées et dont les dimensions sont hauteur et
        largeur
    """    
    if ligne not in range(len(matrice)) or colonne not in range(len(matrice[0])):
        return None
    if hauteur > len(matrice):
        hauteur = len(matrice)
    if largeur > len(matrice[0]):
        largeur = len(matrice[0])
    """ nouvelle_matrice = []
    for i in range(ligne, ligne+hauteur):
        ligne = []
        for j in range(colonne, colonne+largeur):
            ligne.append(matrice[i][j])
        nouvelle_matrice.append(ligne)
    return nouvelle_matrice """
    # pour chaque i entre la ligne demandé et celle-ci + la hauteur demandée
    # on ajoute à notre matrice une ligne contenant pour chaque j entre l'indice de la colonne demandée et celle-ci + la largeur demandée
    # l'élément correspondant à celui de notre matrice originale aux indices i et j
    return [[matrice[i][j] for j in range(colonne,colonne+largeur)] for i in range(ligne,ligne+hauteur)]

def somme(matrice1, matrice2):
    """qui renvoie la matrice résultante de la somme des deux
    matrices passées en paramètre.

    Args:
        matrice1 (liste de listes): une matrice
        matrice2 (liste de listes): une matrice

    Returns:
        matrice (liste de listes): renvoie la matrice résultante de la somme des deux
    matrices passées en paramètre.
    """    
    if len(matrice1) == 0 or len(matrice2) == 0 or len(matrice1) != len(matrice2) or len(matrice1[0]) != len(matrice2[0]):
        return 0
    # tests pour voir si nos matrices sont valides et de même ordre
    
    # pour chaque i correspodant à l'indice d'une ligne de la matrice 1
    # on fabrique une nouvelle ligne
    # pour chaque j correspondant à l'indice de chaque colonne d'une ligne de la matrice 1
    # (on sait que la matrice 1 et 2 sont de même ordre)
    # on rajoute un élément correspondant à l'addition de l'élément aux indices i et j de la matrice 1 et celui de la matrice 2 aux mêmes indices
    return [[matrice1[i][j]+matrice2[i][j] for j in range(len(matrice1[i]))] for i in range(len(matrice1))]
            
def produit(matrice1, matrice2):
    """qui renvoie la matrice résultante u produit des deux
matrices passées en paramètre.

    Args:
        matrice1 (liste de listes): une matrice qui sera multiplié par la deuxième
        matrice2 (liste de listes): une matrice qui multipliera la première

    Returns:
        matrice (liste de listes): renvoie la matrice résultante u produit des deux
matrices passées en paramètre.
    """    
    #on teste si le produit des deux matrices est possible
    if get_nb_colonnes(matrice1) != get_nb_lignes(matrice2):
        return None
    matrice_produit = []
    for i in range(get_nb_lignes(matrice1)):
        # pour chaque ligne de la matrice1 on veut créer une nouvelle ligne pour notre matrice produit avec le même nombre d'éléments que le nombre de colonnes de la matrice 2
        ligne = []
        for k in range(get_nb_colonnes(matrice2)):
            # en rapport avec ce que l'on a dit précédemment, on va parcourir chaque colonne de notre matrice 2
            # cela nous permettra de faire le produit de notre ligne i de la matrice 1 pour chaque colonne k de notre matrice 2 
            valeur = 0
            # on initialise la valeur que l'on donnera à l'élément à 0 car il s'agira de la somme des j produits que l'on fera juste après
            for j in range(get_nb_colonnes(matrice1)):
                # pour chaque j correspondant au numéro de la colonne de la ligne 1, on va ajouter à notre valeur le produit de :
                # 1) matrice1 aux indices i(sa ligne) et j(sa colonne)
                # 2) matrice2 aux indices j(la colonne de matrice1 devient ici la ligne de matrice2) et k(car on veut une valeur pour chaque colonne de matrice2) 
                valeur += matrice1[i][j]*matrice2[j][k]
            ligne.append(valeur) # on ajoute cette valeur à notre ligne
        matrice_produit.append(ligne)
        #on finit alors par ajouter cette ligne à notre matrice produit lorsque'l'on a trouvé toutes les valeurs d'une ligne
    return matrice_produit


# ---------------------------
# fonctions d'affichage
# ---------------------------

def affiche_ligne_separatrice(matrice, taille_cellule=4):
    """fonction auxilliaire qui permet d'afficher (dans le terminal)
    une ligne séparatrice

    Args:
        matrice : une matrice
        taille_cellule (int, optional): la taille d'une cellule. Defaults to 4.
    """
    print()
    for _ in range(get_nb_colonnes(matrice) + 1):
        print('-'*taille_cellule+'+', end='')
    print()


def affiche(matrice, taille_cellule=4):
    """permet d'afficher une matrice dans le terminal

    Args:
        matrice : une matrice
        taille_cellule (int, optional): la taille d'une cellule. Defaults to 4.
    """
    nb_colonnes = get_nb_colonnes(matrice)
    nb_lignes = get_nb_lignes(matrice)
    print(' '*taille_cellule+'|', end='')
    for i in range(nb_colonnes):
        print(str(i).center(taille_cellule) + '|', end='')
    affiche_ligne_separatrice(matrice, taille_cellule)
    for i in range(nb_lignes):
        print(str(i).rjust(taille_cellule) + '|', end='')
        for j in range(nb_colonnes):
            print(str(get_val(matrice, i, j)).rjust(taille_cellule) + '|', end='')
        affiche_ligne_separatrice(matrice, taille_cellule)
    print()