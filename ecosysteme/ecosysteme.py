"""
Init Dev : TP10
Exercice 2 : Ecosystème
"""

def extinction_immediate(ecosysteme, animal):
    """
    renvoie True si animal s'éteint immédiatement dans l'écosystème faute
    de nourriture
    """
    if ecosysteme[animal] == None:
        return False
    return not ecosysteme[animal] in ecosysteme.keys()



def en_voie_disparition(ecosysteme, animal):
    """
    renvoie True si animal s'éteint est voué à disparaitre à long terme
    """
    if extinction_immediate(ecosysteme, animal):
        return True
    # si l'espèce est en extinction immédiate, elle est alors en voie de disparition et on renvoie directement True
    animaux_visites = set()
    # on créé un ensemble d'animaux visités (que l'on remplira au fur et à mesure), si on tombe deux fois sur le même animal, on considère 
    # alors que l'on rentre dans une boucle et que l'animal ne sera pas en voie de disparition
    while not ecosysteme[animal] == None:
        animaux_visites.add(animal)
        # ajout de l'animal dans l'ensemble des animaux visités
        animal = ecosysteme[animal]
        # passage au prochain animal, celui que consomme l'original
        if animal not in ecosysteme.keys():
            #si ce nouvel animal est voué à une extinction immédiate, on retourne True, notre animal d'origine est en voie de disparition 
            return True
        if animal in animaux_visites:
            # ce qu'on a dit l25/26
            return False
    return False

def animaux_en_danger(ecosysteme):
    """ renvoie l'ensemble des animaux qui sont en danger d'extinction immédiate"""
    ensemble_animaux = set()
    for animal in ecosysteme.keys():
        if extinction_immediate(ecosysteme, animal):
            ensemble_animaux.add(animal)
    return ensemble_animaux


def especes_en_voie_disparition(ecosysteme):
    """ renvoie l'ensemble des animaux qui sont en voués à disparaitre à long terme
    """
    ensemble_animaux = set()
    for animal in ecosysteme.keys():
        if en_voie_disparition(ecosysteme, animal):
            ensemble_animaux.add(animal)
    return ensemble_animaux




