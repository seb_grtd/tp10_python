"""Init Dev : TP10"""

# =====================================================================
# Exercice 1 : Choix de modélisation et complexité
# =====================================================================
# Modélisation n°1
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v1 dans le fichier de tests

def appartient_v1(pokemon, pokedex): 
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    for tuple in pokedex: # O(N)
        if pokemon in tuple: #O(1)
            return True
    # Complexité O(N)
    return False

def toutes_les_attaques_v1(pokemon, pokedex): 
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    attaques = set()
    for tuple in pokedex: # O(N)
        if pokemon in tuple: #O(1)
            attaques.add(tuple[1]) #O(1)
    # Complexité O(N)
    return attaques


def nombre_de_v1(attaque, pokedex): 
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    compteur = 0
    for tuple in pokedex: #O(N)
        if attaque in tuple: #O(1)
            compteur +=1
    # Complexité O(N)
    return compteur


def attaque_preferee_v1(pokedex):
    """
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    dic_freq_attaque = {}
    freq_attaque_max = None
    nom_attaque_max = None
    for tuple in pokedex: #O(N)
        if tuple[1] not in dic_freq_attaque.keys(): # O(1)
            dic_freq_attaque[tuple[1]] = 0 #O(1)
        else:
            dic_freq_attaque[tuple[1]] += 1 #o(1)
    for attaque in dic_freq_attaque: #O(N)
        if nom_attaque_max == None or freq_attaque_max < dic_freq_attaque[attaque]: #O(1)
            nom_attaque_max = attaque
            freq_attaque_max = dic_freq_attaque[attaque]
    #Complexite O(N) + O(N) => O(N)
    return nom_attaque_max

# =====================================================================
# Modélisation n°2
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v2 dans le fichier de tests

def appartient_v2(pokemon, pokedex):
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    return pokemon in pokedex.keys() #O(1)

def toutes_les_attaques_v2(pokemon, pokedex):
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    attaques = set()
    if pokemon not in pokedex.keys(): #O(1)
        return attaques
    for attaque_pokemon in pokedex[pokemon]: #O(N)
        attaques.add(attaque_pokemon) #O(1)
    #Complexité O(N)
    return attaques


def nombre_de_v2(attaque, pokedex):
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    compteur = 0
    for type_attaques in pokedex.values(): #O(n)
        if attaque in type_attaques: #O(2) si on considère qu'un pokémon ne peut avoir que deux types
            compteur +=1
    #Complexité O(N)
    return compteur


def attaque_preferee_v2(pokedex):
    """
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    dic_freq_atq = {}
    nom_attaque_max = None
    freq_attaque_max = 0
    for attaques in pokedex.values(): #o(n)
        for attaque in attaques: #o(n)
            if not attaque in dic_freq_atq.keys(): #o(1)
                dic_freq_atq[attaque] = 0 #o(1)
            else:
                dic_freq_atq[attaque] += 1 #o(1)
    for attaque in dic_freq_atq: #O(N)
        if nom_attaque_max == None or freq_attaque_max < dic_freq_atq[attaque]: #o(1)
            nom_attaque_max = attaque
            freq_attaque_max = dic_freq_atq[attaque]
    # Complexité O(N) + O(N) => O(N)
    return nom_attaque_max

# =====================================================================
# Modélisation n°3
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v3 dans le fichier de tests


def appartient_v3(pokemon, pokedex):
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    for pokemons_type in pokedex.values(): #O(N)
        if pokemon in pokemons_type: #O(1)
            return True
    # COMPLEXITE O(N)
    return False


def toutes_les_attaques_v3(pokemon, pokedex):
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    attaques_du_pokemon = set()
    for attaque in pokedex.keys(): #O(N)
        if pokemon in pokedex[attaque]: #O(1)
            attaques_du_pokemon.add(attaque)
    #Complexité O(N)
    return attaques_du_pokemon


def nombre_de_v3(attaque, pokedex):
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    if attaque not in pokedex.keys(): #O(1)
        return 0
    else:
        return len(pokedex[attaque]) #O(1)
    #Complexité O(1)


def attaque_preferee_v3(pokedex):
    """
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    nb_max =0
    atq_max = None
    for type_attaque, pokemons in pokedex.items(): #O(N)
        if atq_max == None or nb_max < len(pokemons): #O(1)
            atq_max = type_attaque
            nb_max = len(pokemons)#O(1)
    #complexite O(N)
    return atq_max

# =====================================================================
# Transformations
# =====================================================================

# Version 1 ==> Version 2

def v1_to_v2(pokedex_v1):
    """
    param: prend en paramètre un pokedex version 1
    renvoie le même pokedex mais en version 2
    """
    dic_pokemon_attaques = {}
    for tuple in pokedex_v1:
        if tuple[0] not in dic_pokemon_attaques.keys():
            dic_pokemon_attaques[tuple[0]] = {tuple[1]}
        else:
            dic_pokemon_attaques[tuple[0]].add(tuple[1])
    return dic_pokemon_attaques
        


# Version 1 ==> Version 2

def v2_to_v3(pokedex_v2):
    """
    param: prend en paramètre un pokedex version2
    renvoie le même pokedex mais en version3
    """
    dic_attaque_pokemons = {}
    for pokemon, attaques in pokedex_v2.items():
        for attaque in attaques:
            if not attaque in dic_attaque_pokemons.keys():
                dic_attaque_pokemons[attaque] = {pokemon}
            else:
                dic_attaque_pokemons[attaque].add(pokemon)
    return dic_attaque_pokemons

# =====================================================================
# Exercice 4: 
# =====================================================================

def pokemons_par_famille(liste_pokemon):
    dic_pokemons = {}
    for tuple in liste_pokemon:
        for type_attaque in tuple[1]:
            if not type_attaque in dic_pokemons.keys():
                dic_pokemons[type_attaque] = {tuple[0]}
            else:
                dic_pokemons[type_attaque].add(tuple[0])
    return dic_pokemons